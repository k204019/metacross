import os
import lxml.etree as ET

inputpath = "/home/k204019/Projects/metacross/data/darus/indir/"
xsltfile = "/home/k204019/Projects/metacross/crosswalks/datacite3.1-to-eudat_v1.0.xsl"
outpath = "/home/k204019/Projects/metacross/data/darus/outdir"

for dirpath, dirnames, filenames in os.walk(inputpath):
    for filename in filenames:
        if filename.endswith(('.xml', '.txt')):
            dom = ET.parse(inputpath + filename)
            assert isinstance(dom, object)
            # print(str((ET.tostring(dom, pretty_print=True))))
            xslt = ET.parse(xsltfile)
            print(str((ET.tostring(xslt, pretty_print=True))))
            transform = ET.XSLT(xslt)
            assert isinstance(transform, object)
            newdom = transform(dom)
            infile = str((ET.tostring(newdom, pretty_print=True)))
            outfile = open(outpath + "/" + filename, 'a')
            outfile.write(infile)
