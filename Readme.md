# METACROSS

This tool is a XML schema based XML converter. It reads data from a source and writes it to a target.

The special part is, that the source to target relation is not one to one, but using an intermediate "core object". The advantage of this intermediate core is, that the number of mappings scales better if multiple inputs or outputs for the same data schema exist. The core object can be dynamically configured, making this tool general purpose and extendable.

### Concept

#### Example Usage (CERA2 read experiments and write to XML)
Mapping of metadata encoded as XML in DataCite metadata schema to XML files formated in EUDAT Core schema. 
The metadata is read from the input XML file formated  in DataCite MDS and mapped into the core object. 
Then in a second step the writer generates the output XML files using the core object as input. 
If multiple XML schemata for the output should be used, only one writer definition for each schema is needed. The reader and core object do not need to be adapted.

The figure shows how this scenario is configured with more details. The configuration `.yml` files in for this scenario can be found in the `configurations` directory. 
The data is then mapped into the core object according to the configuration of the reader. 
In the second step the core objects are fed to the writers and they use templates to generate the output XML files for the given core objects.

![metacross overview](documentation/figures/metacross_overview.png)

##
Just run xsltproc :

metacross/crosswalks $ xsltproc datacite3.1-to-eudat_v1.0.xsl ../testdata/fd8e323f-8c84-59eb-8718-2a850876ed2d.xml 




## Installation

### Conda
1. Install [Anaconda or Miniconda](https://conda.io/projects/conda/en/stable/user-guide/install/index.html) for Python 3 (tested on 3.11) 

#### Automatic
If `make` is installed:

2. Run the install make target

    `make install`


#### Manual
2. Setup the conda environment using

    `conda env create -f environment.yml`

3. Activate the environment

    `conda activate metacross`

4. Update the environment

    `conda env update -f environment.yml -n metacross --prune`

5. Pull the XSD schemas for XML validation

    `./scripts/pull_xsd_schemas.sh`


## Credentials
Credentials are located in the `credentials` directory. The actual credential files are not stored in the git repository, but for each file an `.example` file is located in the credentials directory. The credential files may also be stored in a different place, since the path to the credentials files are configured in the configurations. To run the script the `.example` files must be copied and the suffixes must be removed. Then the placeholders in the files must be filled with the actual values and the script can be executed. If a credentials file should be stored somewhere else, the configurations must be adjusted to represent this change.

**SQL reader:**
The SQL reader configuration contains the `connection_configuration` key, which defines the path to the credentials and connection information for the database relative to the project root.

The file permissions of the credential files should be set as restrictive as possible e.g. read only by the user executing the script.


## Configuration
Detailed configuration for all configurations can be found in the `documentation/configuration`. There are multiple configuration files needed to run the tool. The most important configurations are the configuration of the core object ([object\_core.md](documentation/configuration/core_object.md)) and the configuration of the whole conversion process ([conversion.md](documentation/configuration/conversion.md)). Generally all implemented readers and writers may require a configuration, the configurations are dependent on the implementation of the reader/writer.


## Environment variables
The environment variables are used to change behavior of the tool. The environment variables are used for more static variables of the tool, which do not need to be adapted for each execution.

Example usage:
```
QUERY_RESULTS_COUNT_LIMIT=10 python -m ...
```

**Available variables:**
- `QUERY_RESULTS_COUNT_LIMIT`: The maximum number of query results for any query. (default: 10000)


## Documentation
- Configuration
  - [CoreObject](documentation/configuration/core_object.md)
  - [SQLReader](documentation/configuration/reader_sql.md)
  - [TemplatedTextWriter](documentation/configuration/writer_templated_text.md)

- [Development information](documentation/development.md)

## Execute
The `main.py` script in the project root is used to run the tool. There are multiple command line arguments required to be able execute the tool.

Sample command to generate Dublin Core XMLs for all experiments:
```shell
python main.py \
    -c configurations/core_object_oai.yml \
    -r configurations/reader_cera2.yml \
    -w configurations/writer_dublin_core.yml
    --writer-output-base-directory path/to/directory
```

The available arguments can be listed using `python main.py -h`:
- `-c` / `--core-config` Path to core object configuration file
- `-r` / `--reader-config` Path to reader configuration file
- `-w` / `--writer-config` Path to writer configuration file

There may be special arguments required for some readers and writers, they are prefixed with `reader_` or `writer_`:
- `-o` / `--writer-output-base-directory` Base directory for the writer to write files to. (required for: `TemplatedTextWriter`)




## Development
The `Makefile` in the project root contains targets for most commands required in the development workflow.

- `make format`: Run the `black` Python code formatter in place
- `make type_check`: Run `mypy` type checker
- `make test`: Run all unit tests recording the code coverage using `coverage`
- `make code_coverage`: Generate the code coverage html overview pages into the `htmlcov` directory
- `make show_coverage`: Open the `htmlcov/index.html` in the browser (default application for html files)
- `make install_dependencies`: Installs/updates the dependencies required to run this project
- `make pull_xsd`: Pulls the XSD schemas for schema validation, the script `scripts/pull_xsd_schemas.sh` can also be executed manually

For more information on how the project is structured see [development.md](documentation/development.md).

## Deployment / Server
See [documentation/deployment.md](documentation/deployment.md)

### Issues with MSCR xsd-csv conversion
- missing columns: 
-- occurence (0-1, 1, 0-n...also covers if element is mandatory), 
-- controlled vocabularies,
-- field vs. attribute (example <title> and <titletype>)
- what about 'envelopes' like <titles><title><titles>?
- where do we comment?
- in the following : specific typing - maybe data types for the DTR ???
-- 21;doiType;
-- 22;yearType;defines the value for a year : pattern value="[\d]{4}"
-- 23;point;definitions for geoLocation / restriction base="listOfDoubles"
-- 23.1;minLength
-- 23.2;maxLength;value="2"
-- 24;box;definitions for geoLocation (box) / restriction base="listOfDoubles"
-- 24.1;minLength;value="4"
-- 24.2;maxLength;value="4"
- how to mask ';' in description
- identifier syntax and hierarchy ( e.g. 1.Identifier / 1.1. identifierType ) ?