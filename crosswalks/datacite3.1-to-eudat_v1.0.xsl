<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:eudatcore="http://schema.eudat.eu/schema/kernel-1"
                version="1.0">

<xsl:output method="xml" encoding="utf-8" indent="yes"/>
<xsl:param name="eudatcore">http://schema.eudat.eu/schema/kernel-1</xsl:param>

<!--

  Master template
  ===============



  <xsl:template match="/">
    <rdf:RDF>
      <xsl:apply-templates select="*[local-name() = 'resource' and starts-with(namespace-uri(), 'http://datacite.org/schema/kernel-')]|//*[local-name() = 'resource' and starts-with(namespace-uri(), 'http://datacite.org/schema/kernel-')]"/>
    </rdf:RDF>
  </xsl:template>
 -->


<!--<xsl:template match="/">-->
<xsl:template match="*[local-name() = 'resource' and starts-with(namespace-uri(), 'http://datacite.org/schema/kernel-')]|//*[local-name() = 'resource' and starts-with(namespace-uri(), 'http://datacite.org/schema/kernel-')]">
  <eudatcore:identifier>
    <xsl:value-of select="//*[local-name()='Identifier']"/>
  </eudatcore:identifier>
  <eudatcore:identifierType>
    <xsl:value-of select="//@identifierType"/>
  </eudatcore:identifierType>
  <eudatcore:creator>
    <xsl:value-of select="//*[local-name()='Creator']"/>
  </eudatcore:creator>
  <eudatcore:creatorName>
    <xsl:value-of select="//*[local-name()='creatorName']"/>
  </eudatcore:creatorName>
  <eudatcore:title>
    <xsl:value-of select="//*[local-name()='Title']"/>
  </eudatcore:title>
  <eudatcore:Publisher>
    <xsl:value-of select="//*[local-name()='publisher']"/>
  </eudatcore:Publisher>
  <eudatcore:PublicationYear>
    <xsl:value-of select="//*[local-name()='publicationYear']"/>
  </eudatcore:PublicationYear>
  <eudatcore:tags>
    <xsl:value-of select="//*[local-name()='subject']"/>
  </eudatcore:tags>
  <eudatcore:Contributor>
    <xsl:value-of select="//*[local-name()='contributorName']"/>
  </eudatcore:Contributor>
<!-- <xsl:copy-of select="*" /> -->
</xsl:template>

</xsl:stylesheet>