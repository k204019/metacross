<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:datacite="http://datacite.org/schema/kernel-4">

<xsl:output method="xml" encoding="utf-8" indent="no"/>

<xsl:template match="/">
  <datacite:subject>
    <xsl:value-of select="//*[local-name()='title']"/>
  </datacite:subject>

<xsl:copy-of select="*" />
</xsl:template>

</xsl:stylesheet>