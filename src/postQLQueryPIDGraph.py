import requests
import json

def qlgraphQuery(doi):
    # GraphQL endpoint URL
    graphql_url = "https://api.datacite.org/graphql"

    # GraphQL query
    graphql_query = """
    {dataset(id: \"%s\" ) 
      {
        id
        titles
        {
          title
        }
        dates
        {
          date
        }
      }
    }
    """ %(doi)

    # Set the headers for the request (specifying JSON content type)
    headers = {
        "Content-Type": "application/json"
    }

    # Prepare the request payload
    graphql_payload = {
        "query": graphql_query
    }

    response_data = None
    try:
        # Make the GraphQL API call with a POST request
        response = requests.post(graphql_url, json=graphql_payload, headers=headers)

        # Check if the request was successful (status code 200)
        if response.status_code == 200:
            # Parse and print the response JSON data
            response_data = response.json()
            formatted_json = json.dumps(response_data, indent=4)
            print("Response:", formatted_json)
        else:
            print("Error:", response.status_code, response.text)

    except requests.exceptions.RequestException as e:
        print("Error:", e)

    return response_data


ckan_api_url = "https://b2find.eudat.eu/api/3/action/package_search"

# CKAN API key (if authentication is required)
api_key = "YOUR_CKAN_API_KEY"

# CKAN API parameters
search_term = "datasets"
rows = 1  # Number of rows to retrieve

# Prepare the request headers
headers = {
    "Authorization": api_key
}

# Prepare the request parameters
params = {
    "q": search_term,
    "rows": rows
}

try:
    # Make the CKAN API call with a GET request
    b2fResponse = requests.get(ckan_api_url, headers=headers, params=params)

    # Check if the request was successful (status code 200)
    if b2fResponse.status_code == 200:
        # Parse and print the b2fResponse JSON data
        b2fResponse_data = b2fResponse.json()
        records = b2fResponse_data.get("result", {}).get("results", [])
        #print("Records:", records)
        for record in records :
            if 'value' in record["extras"][1]:
                doi=record["extras"][1]['value']
                print("  DOI :",doi)
                formatted_json = json.dumps(record, indent=4)
                print(formatted_json)
                qlRecord = qlgraphQuery(doi)
            else:
                print(" record has no DOI ")
                continue

    else:
        print("Error:", b2fResponse.status_code, b2fResponse.text)

except requests.exceptions.RequestException as e:
    print("Error:", e)